<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index () {
        return view('posts.index');
    }

    public function create () {
        return view('posts.create');
    }

    public function individual ($id, $category = "No se escribio nada") {
        return view('posts.show', compact('id', 'category'));
        // return view('posts.show', [
        //     'id' => $id,
        //     'category' => $category,
        // ]);
        // return "Hola ".$id.". Categoria -> ".$category;
    }
}
