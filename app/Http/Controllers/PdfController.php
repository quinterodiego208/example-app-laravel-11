<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class PdfController extends Controller
{
    public function index () {
        $users = User::get();
    
        $data = [
            'title' => 'Welcome to ItSolutionStuff.com',
            'date' => date('m/d/Y'),
            'content' => $users
        ]; 

        $html = '<!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Document</title>
            <style>
                .page-break {
                    page-break-after: always;
                }
            </style>
        </head>
        <body>
            <h1>Holaaaa</h1>
            <a href="www.google.com">A Google </a>
            <div class="page-break"></div>
            <h1>Page 2</h1>
        </body>
        </html>';

        //Una manera        
        // $pdf = Pdf::loadView('pdf.document', $data);
        // return $pdf->download('doc.pdf');
        
        //Una manera con HTML
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadHTML($html);
        return $pdf->stream();

        // Load HTML content
        //  $html = view('pdf.document', $data)->render();

        //  // Instantiate Dompdf
        //  $dompdf = new Dompdf();
        //  $dompdf->loadHtml($html);

        //  // Set paper size and orientation
        //  $dompdf->setPaper('A4', 'portrait');

        //  // Render PDF (important step!)
        //  $dompdf->render();

        //  // Output PDF to browser
        //  return $dompdf->stream('document.pdf');
    }
}
