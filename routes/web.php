<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PdfController;
use App\Http\Controllers\PostController;
use App\Mail\PostCreatedMail;
use App\Models\Post;
use Illuminate\Support\Facades\Mail;

Route::get('/', HomeController::class);

Route::get('/prueba', function () {

    // $post = Post::find(2);
    // $post->delete();
    // return "Eliminado";

    // $posts = Post::all();
    // $posts = Post::where('id','<=','2')->get();
    // $posts = Post::orderBy('id','desc')->select('id','title','categoria')->take(1)->get();
    // return $posts;

    // $post = Post::where('title', 'Titulo de prueba 1')->first();
    // $post->title = 'Desarrollo web';
    // $post->save();
    // return $post;

    // $post = Post::find(1);
    // return $post;

    // $posts = DB::select('select * from users');
    // DB::statement("CREATE DATABASE pruebita");
    // $statement = DB::statement("USE users");
    // DB::statement("SELECT * FROM usuarios");
    
    $post = new Post;
    $post->title = 'TituLO DE prueBA 1';
    $post->content = 'Contenido de prueba 1';
    $post->categoria = 'Categoria de prueba 1';
    // $post->save();
    return $post;

});

Route::get('/email', function () {
    // Mail::to('quinterodiego208@gmail.com')->send(new PostCreatedMail);
    Mail::to('lol@trusting-visvesvaraya.162-222-203-103.plesk.page')->send(new PostCreatedMail);

    return "Se envio el correo!!";
});

Route::get('/pdf', [PdfController::class, 'index']);

Route::get('/posts', [PostController::class, 'index']);
Route::get('/posts/create', [PostController::class, 'create']);
Route::get('/posts/{id}/{category?}', [PostController::class, 'individual']);
